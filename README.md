<div align="center">

# Profile
<div align="right">
  <p>경기 수원시 곡반정로158</p>
  <p>baekkie_song@gmail.com</p>
  <p>[My git documents repository](https://gitlab.com/BaekkieSong/documents/-/milestones)</p>   

  **송백기**
</div>
</div>

## 핵심 스킬
* C++ 중심의 Application 설계 및 구현
* 오픈소스 브라우저를 기반으로 멀티플래폼 환경에서의 제품 개발
* 풀스택 레벨의 기술 경험 (웹 프론트, 클라이언트 앱, 릴리즈/동기화 서버 구축 및 운영, CI/CD 등)
* 구글 코딩 스타일에 맞춰 여러 언어 사용 경험
* Experience
  * Language
    * `c++`, `Python`, `NodeJS`
    * `Java with JNI`, `objc`
  * Tools
    * `vim` `CMake build` `gitlab CI` `Docker` `Google tools(gn/gTest/gRPC/Protobuf)` `MariaDB` `Sqlite` `Valgrind`
  * Platform
    * `TmaxOS` `Linux(Ubuntu, Debian)` `Windows` `MacOS` `Android` `iOS`
  * Develop Framework
    * `Linux Shell` `VSCode` `Visual Studio` 
## 업무 경험
* Tmax A&C 재직
  * 데스크톱/모바일 통합 웹 앱 프레임워크 개발 (2021.6 ~ 2022.8)
    - iOS용 Chromium 렌더링 엔진 빌드, iOS Device환경 기준으로 웹페이지 UI 테스트 완료
    - Android용 Chromium 렌더링 엔진 라이브러리 빌드 및 앱 동작 테스트
  * 회사 OS용 브라우저 연구개발 (2018.11~2021.5)
    - Chromium 최신 버전 포팅
    - 브라우저 UI 설계 및 구현
    - Chromium 렌더링 엔진 분석
    - 앱 패키징 및 배포를 위한 스크립트 적용
    - 업데이트 서버 구현, 최신 버전 패키지 배포시 앱을 자동 업데이트하는 로직 구현
    - 사용자 데이터 동기화 서버 구현
    - 회사 DevOps를 이용한 서버 관리 자동화 적용
    - Gitlab 서버 관리, CI/CD를 활용해 빌드 테스트, 브랜치, 태그 관리 등 자동화 적용
  * 회사 OS용 기본앱 연구개발 (2017.08 ~ 2018.10)
    - 계산기 리팩토링(일반 계산기 실시간 계산 알고리즘 적용, 공학용 모드 구현)
    - 캡처도구 리팩토링(캡처 모드별 독립적인 동작을 보장할 수 있도록 아키텍쳐 재설계, Drawing 기능 추가)
    - 메모장 리팩토링(UI 뷰포트 부분 업데이트 방식을 적용하여 이동/편집 성능 향상, 클립보드 오류 개선)
    - 스티커메모 구현(UI 가이드에 따라 아키텍쳐 설계 및 구현)
    - Gitlab서버 관리 및 CI/CD를 이용한 빌드 테스트, 패키지 배포 자동화 로직 구현하여 적용
## 학력
* 경희대학교 전자전파공학 석사 졸업 (2015.9 ~ 2017.8)
  * 석사 학위 논문
    * 포트홀 검출을 위한 신경망 하드웨어 구조
  * 주요 연구 과제
    * FPGA 보드를 이용해 HD영상에서 실시간으로 얼굴을 검출하는 알고리즘 연구
    * 신경망을 하드웨어 언어로 설계하여 검출 속도 향상하는 방안 연구
* 경희대학교 전자전파공학 학사 졸업(2009.3 ~ 2015.8)
